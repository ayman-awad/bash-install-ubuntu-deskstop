#!/bin/bash

message_start='Start install tools and soft'
message_end='Succed !!'

echo $message




case "$1" in
    dev)
        sudo snap install android-studio --classic
        sudo snap install postman
        sudo snap install pycharm-professional --classic
        sudo snap install phpstorm --classic
        sudo snap install intellij-idea-ultimate --classic
        sudo snap install kotlin-native --classic
        sudo snap install kotlin --classic
        sudo snap install go --classic
        sudo snap install goland --classic
        sudo snap install rider --classic
        sudo snap install datagrip --classicsudo 
        snap install dotnet-sdk --classic
        ;;
    utils-dev)
        sudo snap install gimp
        sudo snap install audacity
        sudo snap install mailspring
        sudo snap install onlyoffice-desktopeditors 
        sudo apt-get install guake -y
        sudo apt install composer
        ;;    
    utils)
        sudo snap install spotify
        sudo snap install gimp
        sudo snap install audacity
        sudo snap install mailspring
        sudo snap install onlyoffice-desktopeditors 
        sudo apt-get install guake -y
        sudo apt-get install snapd
        sudo snap install snap-store
        sudo snap install notepad-plus-plus
        sudo snap install discord
    ;;      
esac